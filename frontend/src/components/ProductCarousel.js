import React, { useEffect } from 'react'
import { Carousel, Image } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { listTopProducts } from '../actions/productActions'
import Loader from './Loader'
import Message from './Message'
import bannerImg from '../images/groceryBanner.png'

const ProductCarousel = () => {
  const dispatch = useDispatch()

  const productTopRated = useSelector((state) => state.productTopRated)
  const { loading, error, products } = productTopRated

  useEffect(() => {
    dispatch(listTopProducts())
  }, [dispatch])

  return loading ? (
    <Loader />
  ) : error ? (
    <Message variant='danger'>{error}</Message>
  ) : (
    <div>
      <div className="container">
        <img src={bannerImg} alt="banner" />
      </div>
    </div>
    // <Carousel pause='hover' className='bg-white'>
    //   {products.map((product) => (
    //     <Carousel.Item key={product._id}>
    //         <Image src={product.image} alt={product.name} fluid />
    //     </Carousel.Item>
    //   ))}
    // </Carousel>
  )
}

export default ProductCarousel
