import React, { useState } from 'react'
import { Button, Card, Form } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { addToCart } from '../actions/cartActions'
import Message from './Message'

const Product = ({ product }) => {
  const [qty, setQty] = useState(1);
  const [show, setShow] = useState(false);
  const dispatch = useDispatch()
  const addToCartHandler = (productId, qty) => {
    dispatch(addToCart(productId, Number(qty)))
    setShow(true);
  }
  return (
    <>
      <Card className='my-3 p-2 rounded productItem'>
        <Card.Img src={product.image} variant='top' />

        <Card.Body>

          <Card.Title as='div'>
            <strong>{product.name}</strong>
          </Card.Title>
          <Card.Text>1 l</Card.Text>
          <Card.Text as='h3'><i class="fas fa-rupee-sign"></i> {product.price} <span>{product.price}</span></Card.Text>

          <Card.Text as='div' className="row">
            {/* <div className='col-md-12'>
              <Form.Control
                as='input'
                value={qty}
                onChange={(e) => setQty(e.target.value)}
                size="sm"
              ></Form.Control>
            </div> */}
            <div className='col-md-12'>
              <Button
                onClick={() => { addToCartHandler(product._id, qty) }}
                type='button'
                disabled={product?.countInStock === 0}
                size="sm"
                class="flex between"
              >
                <span>Add to Cart</span> <i class="fas fa-plus"></i>
              </Button>
            </div>
          </Card.Text>
        </Card.Body>
      </Card>
      {show && <Message variant='success'>Added to cart</Message>}
    </>
  )
}

export default Product
