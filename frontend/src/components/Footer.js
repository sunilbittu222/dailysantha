import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

const Footer = () => {
  return (
    <footer>
      <Container>
        <Row className="flex between">
          <h6>Copyright &copy; Dailysantha</h6>
          <ul className="flex">
            <li>
              <a href="#">About</a>
            </li>
            <li>
              <a href="#">Terms and Conditions</a>
            </li>
            <li>
              <a href="#">Disclimer</a>
            </li>
            <li>
              <a href="#">Privacy Policy</a>
            </li>
          </ul>
        </Row>
      </Container>
    </footer>
  )
}

export default Footer
