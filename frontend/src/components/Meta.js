import React from 'react'
import { Helmet } from 'react-helmet'

const Meta = ({ title, description, keywords }) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name='description' content={description} />
      <meta name='keyword' content={keywords} />
    </Helmet>
  )
}

Meta.defaultProps = {
  title: 'Welcome To Dailysantha',
  description: 'We sell the best groceries for cheap',
  keywords: 'grocery, buy grocery, cheap groceries, vegitables, cheap',
}

export default Meta
